package eu.proszowski.democalculator;

public enum Event {
    CALCULATION_CHANGED, MODE_CHANGED
}
