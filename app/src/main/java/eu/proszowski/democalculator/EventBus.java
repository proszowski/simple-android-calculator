package eu.proszowski.democalculator;

import java.util.HashMap;
import java.util.Map;

public class EventBus {

    private Map<Event, Action> actions = new HashMap<>();

    public EventBus() {
    }

    public void emit(Event event) {
        Action action = actions.get(event);
        if(action != null){
            action.execute();
        }
    }

    public void subscribe(Event event, Action taskToExecute) {
        actions.put(event, taskToExecute);
    }
}
