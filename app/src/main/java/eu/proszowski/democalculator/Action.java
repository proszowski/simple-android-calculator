package eu.proszowski.democalculator;

public interface Action {
    void execute();
}