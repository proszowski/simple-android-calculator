package eu.proszowski.democalculator.ui;

import android.os.Bundle;

import javax.inject.Inject;

import eu.proszowski.democalculator.CalculatorFacade;
import eu.proszowski.democalculator.EventBus;
import eu.proszowski.democalculator.R;

public class SimpleModeActivity extends CalculatorActivity {

    @Inject
    CalculatorFacade calculatorFacade;

    @Inject
    EventBus eventBus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_mode);
    }

}
