package eu.proszowski.democalculator.ui;

import android.os.Bundle;

import eu.proszowski.democalculator.R;

public class AdvancedModeActivity extends CalculatorActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advanced_mode);
    }
}
