package eu.proszowski.democalculator.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import eu.proszowski.democalculator.CalculatorFacade;
import eu.proszowski.democalculator.Event;
import eu.proszowski.democalculator.EventBus;
import eu.proszowski.democalculator.R;
import eu.proszowski.democalculator.command.CalculatorCommand;
import eu.proszowski.democalculator.command.FactorialCommand;
import eu.proszowski.democalculator.command.LogarithmCommand;
import eu.proszowski.democalculator.command.NaturalLogarithmCommand;
import eu.proszowski.democalculator.command.PercentageCommand;
import eu.proszowski.democalculator.command.SquareRootCommand;

import static eu.proszowski.democalculator.Event.CALCULATION_CHANGED;

public class AdvancedUpSideButtonsFragment extends DaggerFragment {

    @Inject
    CalculatorFacade calculatorFacade;
    @Inject
    EventBus eventBus;
    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.advanced_up_side_buttons, container, false);
        Button logarithmButton = view.findViewById(R.id.LogarithmButton);
        logarithmButton.setOnClickListener(this::calculateLogarithm);
        Button naturalLogarithmCommand = view.findViewById(R.id.NaturalLogarithmButton);
        naturalLogarithmCommand.setOnClickListener(this::calculateNaturalLogarithm);
        Button squareRootButton = view.findViewById(R.id.SquareRootButton);
        squareRootButton.setOnClickListener(this::calculateSquareRoot);
        Button factorialButton = view.findViewById(R.id.FactorialButton);
        factorialButton.setOnClickListener(this::calculateFactorial);
        Button percentageButton = view.findViewById(R.id.PercentageButton);
        percentageButton.setOnClickListener(this::calculatePercentage);
        return view;
    }

    private void calculateLogarithm(View view) {
        LogarithmCommand logarithmCommand = new LogarithmCommand();
        calculatorFacade.applyCommand(logarithmCommand);
        eventBus.emit(CALCULATION_CHANGED);
    }

    private void calculateNaturalLogarithm(View view) {
        NaturalLogarithmCommand naturalLogarithmCommand = new NaturalLogarithmCommand();
        calculatorFacade.applyCommand(naturalLogarithmCommand);
        eventBus.emit(CALCULATION_CHANGED);
    }

    private void calculateSquareRoot(View view) {
        SquareRootCommand squareRootCommand = new SquareRootCommand();
        calculatorFacade.applyCommand(squareRootCommand);
        eventBus.emit(CALCULATION_CHANGED);
    }

    private void calculateFactorial(View view) {
        FactorialCommand factorialCommand = new FactorialCommand();
        calculatorFacade.applyCommand(factorialCommand);
        eventBus.emit(CALCULATION_CHANGED);
    }

    private void calculatePercentage(View view) {
        CalculatorCommand percentageCommand = new PercentageCommand();
        calculatorFacade.applyCommand(percentageCommand);
        eventBus.emit(CALCULATION_CHANGED);
    }
}
