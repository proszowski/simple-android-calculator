package eu.proszowski.democalculator.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import eu.proszowski.democalculator.CalculationTextFormatter;
import eu.proszowski.democalculator.CalculatorFacade;
import eu.proszowski.democalculator.Event;
import eu.proszowski.democalculator.EventBus;
import eu.proszowski.democalculator.R;

public class DisplayerFragment extends DaggerFragment {

    @Inject
    CalculatorFacade calculatorFacade;
    @Inject
    EventBus eventBus;
    @Inject
    CalculationTextFormatter calculationTextFormatter;

    private View view;
    private TextView displayer;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.displayer, container, false);
        displayer = view.findViewById(R.id.Displayer);
        update();
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        eventBus.subscribe(Event.CALCULATION_CHANGED, this::update);
        eventBus.subscribe(Event.MODE_CHANGED, this::changeFormatter);
    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
    }

    private void changeFormatter() {
        calculationTextFormatter.changeMode();
    }

    public void update() {
        displayer.setText(calculatorFacade.getDisplayerText(calculationTextFormatter));
    }
}
