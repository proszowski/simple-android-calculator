package eu.proszowski.democalculator.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import eu.proszowski.democalculator.CalculatorFacade;
import eu.proszowski.democalculator.Event;
import eu.proszowski.democalculator.EventBus;
import eu.proszowski.democalculator.R;
import eu.proszowski.democalculator.command.AppendDigitCommand;
import eu.proszowski.democalculator.command.AppendDotCommand;
import eu.proszowski.democalculator.command.CalculatorCommand;
import eu.proszowski.democalculator.command.ClearDisplayerCommand;
import eu.proszowski.democalculator.command.DivisionCommand;
import eu.proszowski.democalculator.command.EqualsCommand;
import eu.proszowski.democalculator.command.MultiplyCommand;
import eu.proszowski.democalculator.command.OppositeCommand;
import eu.proszowski.democalculator.command.RemoveLastSignCommand;
import eu.proszowski.democalculator.command.SubtractCommand;
import eu.proszowski.democalculator.command.SumCommand;
import eu.proszowski.democalculator.domain.Digit;

import static eu.proszowski.democalculator.Event.CALCULATION_CHANGED;

public class SimpleModeButtonsFragment extends DaggerFragment {

    @Inject
    CalculatorFacade calculatorFacade;

    @Inject
    EventBus eventBus;

    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.simple_mode_buttons, container, false);
        initializeEventHandlers();

        return view;
    }

    private void initializeEventHandlers() {
        List<Button> digitButtons = Arrays.asList(view.findViewById(R.id.ZeroButton),
                view.findViewById(R.id.OneButton), view.findViewById(R.id.TwoButton),
                view.findViewById(R.id.ThreeButton), view.findViewById(R.id.FourButton),
                view.findViewById(R.id.FiveButton), view.findViewById(R.id.SixButton),
                view.findViewById(R.id.SevenButton), view.findViewById(R.id.EightButton),
                view.findViewById(R.id.NineButton)
        );

        for (Button digitButton : digitButtons) {
            digitButton.setOnClickListener((view) -> appendDigit(digitButton));
        }

        Button dotButton = view.findViewById(R.id.DotButton);
        dotButton.setOnClickListener((view) -> appendDot());

        ImageButton backspaceButton = view.findViewById(R.id.BackspaceButton);
        backspaceButton.setOnClickListener((view) -> removeLastSign());

        Button clearDisplayerButton = view.findViewById(R.id.ClearButton);
        clearDisplayerButton.setOnClickListener((view) -> clearDisplayer());

        Button additionOperationButton = view.findViewById(R.id.AdditionButton);
        additionOperationButton.setOnClickListener((view) -> appendAdditionOperation());

        Button equalsButton = view.findViewById(R.id.EqualsButton);
        equalsButton.setOnClickListener((view) -> equals());

        Button subtractButton = view.findViewById(R.id.MinusButton);
        subtractButton.setOnClickListener((view) -> subtract());

        Button multiplyButton = view.findViewById(R.id.MultiplicationButton);
        multiplyButton.setOnClickListener((view) -> multiply());

        Button divideButton = view.findViewById(R.id.DivisionButton);
        divideButton.setOnClickListener((view) -> divide());

        Button oppositeButton = view.findViewById(R.id.OppositeButton);
        oppositeButton.setOnClickListener((view) -> opposite());

        ImageButton switchButton = view.findViewById(R.id.SwitchButton);
        switchButton.setOnClickListener((view) -> switchModes());
    }

    public void appendDigit(Button button) {
        String digit = button.getText().toString();
        CalculatorCommand appendDigitCommand = new AppendDigitCommand(Digit.from(Integer.valueOf(digit)));
        calculatorFacade.applyCommand(appendDigitCommand);
        eventBus.emit(CALCULATION_CHANGED);
    }

    public void removeLastSign() {
        CalculatorCommand removeLastSignCommand = new RemoveLastSignCommand();
        calculatorFacade.applyCommand(removeLastSignCommand);
        eventBus.emit(CALCULATION_CHANGED);
    }

    public void clearDisplayer() {
        CalculatorCommand clearDisplayerCommand = new ClearDisplayerCommand();
        calculatorFacade.applyCommand(clearDisplayerCommand);
        eventBus.emit(CALCULATION_CHANGED);
    }

    public void appendDot() {
        CalculatorCommand appendDotCommand = new AppendDotCommand();
        calculatorFacade.applyCommand(appendDotCommand);
        eventBus.emit(CALCULATION_CHANGED);
    }

    public void appendAdditionOperation() {
        CalculatorCommand sumCommand = new SumCommand();
        calculatorFacade.applyCommand(sumCommand);
        eventBus.emit(CALCULATION_CHANGED);
    }

    public void equals() {
        CalculatorCommand equalsCommand = new EqualsCommand();
        calculatorFacade.applyCommand(equalsCommand);
        eventBus.emit(CALCULATION_CHANGED);
    }

    public void subtract() {
        CalculatorCommand subtractCommand = new SubtractCommand();
        calculatorFacade.applyCommand(subtractCommand);
        eventBus.emit(CALCULATION_CHANGED);
    }

    public void multiply() {
        CalculatorCommand multiplyCommand = new MultiplyCommand();
        calculatorFacade.applyCommand(multiplyCommand);
        eventBus.emit(CALCULATION_CHANGED);
    }

    public void divide() {
        CalculatorCommand divisionCommand = new DivisionCommand();
        calculatorFacade.applyCommand(divisionCommand);
        eventBus.emit(CALCULATION_CHANGED);
    }

    private void opposite() {
        CalculatorCommand oppositeCommand = new OppositeCommand();
        calculatorFacade.applyCommand(oppositeCommand);
        eventBus.emit(CALCULATION_CHANGED);
    }

    private void switchModes() {
        Activity activity = getActivity();
        Class<? extends Activity> aClass = activity instanceof SimpleModeActivity ? AdvancedModeActivity.class :
                SimpleModeActivity.class;
        Intent intent = new Intent(activity, aClass);
        activity.startActivity(intent);
        activity.overridePendingTransition(0, 0);
        eventBus.emit(Event.MODE_CHANGED);
    }
}
