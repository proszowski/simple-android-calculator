package eu.proszowski.democalculator.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import eu.proszowski.democalculator.CalculatorFacade;
import eu.proszowski.democalculator.Event;
import eu.proszowski.democalculator.EventBus;
import eu.proszowski.democalculator.R;
import eu.proszowski.democalculator.command.AppendDigitCommand;
import eu.proszowski.democalculator.command.CosinusCommand;
import eu.proszowski.democalculator.command.PowerOfYCommand;
import eu.proszowski.democalculator.command.SinusCommand;
import eu.proszowski.democalculator.command.TangesCommand;
import eu.proszowski.democalculator.domain.Digit;

public class AdvancedLeftSideButtonsFragment extends DaggerFragment {

    @Inject
    CalculatorFacade calculatorFacade;
    @Inject
    EventBus eventBus;
    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.advanced_left_side_buttons, container, false);
        Button sinusButton = view.findViewById(R.id.SinButton);
        sinusButton.setOnClickListener((view) -> calculateSinus());
        Button cosinusButton = view.findViewById(R.id.CosButton);
        cosinusButton.setOnClickListener((view) -> calculateCosinus());
        Button tangesButton = view.findViewById(R.id.TangesButton);
        tangesButton.setOnClickListener((view) -> calculateTanges());
        Button powerOfYButton = view.findViewById(R.id.PowerOfYButton);
        powerOfYButton.setOnClickListener(this::calculatePowerOfY);
        Button powerOfTwoButton = view.findViewById(R.id.PowerOfTwoButton);
        powerOfTwoButton.setOnClickListener(this::calculatePowerOfTwo);
        return view;
    }

    private void calculateSinus() {
        SinusCommand sinusCommand = new SinusCommand();
        calculatorFacade.applyCommand(sinusCommand);
        eventBus.emit(Event.CALCULATION_CHANGED);
    }

    private void calculateCosinus() {
        CosinusCommand cosinusCommand = new CosinusCommand();
        calculatorFacade.applyCommand(cosinusCommand);
        eventBus.emit(Event.CALCULATION_CHANGED);
    }

    private void calculateTanges() {
        TangesCommand tangesCommand = new TangesCommand();
        calculatorFacade.applyCommand(tangesCommand);
        eventBus.emit(Event.CALCULATION_CHANGED);
    }

    private void calculatePowerOfY(View view) {
        PowerOfYCommand powerOfYCommand = new PowerOfYCommand();
        calculatorFacade.applyCommand(powerOfYCommand);
        eventBus.emit(Event.CALCULATION_CHANGED);
    }

    private void calculatePowerOfTwo(View view) {
        PowerOfYCommand powerOfYCommand = new PowerOfYCommand();
        calculatorFacade.applyCommand(powerOfYCommand);
        AppendDigitCommand appendDigitCommand = new AppendDigitCommand(Digit.TWO);
        calculatorFacade.applyCommand(appendDigitCommand);
        eventBus.emit(Event.CALCULATION_CHANGED);
    }
}
