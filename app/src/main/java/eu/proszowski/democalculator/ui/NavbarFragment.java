package eu.proszowski.democalculator.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import eu.proszowski.democalculator.Event;
import eu.proszowski.democalculator.EventBus;
import eu.proszowski.democalculator.R;

public class NavbarFragment extends DaggerFragment {

    @Inject
    EventBus eventBus;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.navbar, container, false);

        Button aboutMenuButton = view.findViewById(R.id.AboutMenuButton);
        Button simpleMenuButton = view.findViewById(R.id.SimpleMenuButton);
        Button advancedMenuButton = view.findViewById(R.id.AdvancedMenuButton);

        aboutMenuButton.setOnClickListener(new NavbarOnClickListener(AboutActivity.class));
        simpleMenuButton.setOnClickListener(new NavbarOnClickListenerWithModeChangedEvent(SimpleModeActivity.class));
        advancedMenuButton.setOnClickListener(new NavbarOnClickListenerWithModeChangedEvent(AdvancedModeActivity.class));

        Button buttonToColor = getButtonToColor();
        setMenuButtonColor(buttonToColor);

        return view;
    }

    private Button getButtonToColor() {
        Activity activity = getActivity();
        if (activity instanceof SimpleModeActivity) {
            return view.findViewById(R.id.SimpleMenuButton);
        } else if (activity instanceof AdvancedModeActivity) {
            return view.findViewById(R.id.AdvancedMenuButton);
        } else {
            return view.findViewById(R.id.AboutMenuButton);
        }
    }

    public void setMenuButtonColor(Button button) {
        List<Button> menuButtons = Arrays.asList(view.findViewById(R.id.SimpleMenuButton),
                view.findViewById(R.id.AdvancedMenuButton),
                view.findViewById(R.id.AboutMenuButton));
        for (Button menuButton : menuButtons) {
            menuButton.setTextColor(Color.BLACK);
        }
        TypedValue typedValue = new TypedValue();
        getActivity().getTheme().resolveAttribute(R.attr.colorAccent, typedValue, true);
        int color = typedValue.data;
        button.setTextColor(color);
    }

    private class NavbarOnClickListener implements View.OnClickListener {

        private Class<?> aClass;

        public NavbarOnClickListener(Class<?> aClass) {
            this.aClass = aClass;
        }

        @Override
        public void onClick(View v) {
            Activity activity = getActivity();
            Intent intent = new Intent(activity, aClass);
            if (isNotInDesiredMode(activity)) {
                activity.startActivity(intent);
                activity.overridePendingTransition(0, 0);
            }
        }

        private boolean isNotInDesiredMode(Context context) {
            return !(context.getClass().equals(aClass));
        }
    }

    private class NavbarOnClickListenerWithModeChangedEvent extends NavbarOnClickListener{

        public NavbarOnClickListenerWithModeChangedEvent(Class<?> aClass) {
            super(aClass);
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
            eventBus.emit(Event.MODE_CHANGED);
        }
    }
}
