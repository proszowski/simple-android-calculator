package eu.proszowski.democalculator.ui;

import android.os.Bundle;
import android.view.Window;

import dagger.android.support.DaggerAppCompatActivity;

abstract class CalculatorActivity extends DaggerAppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
    }

}
