package eu.proszowski.democalculator;

import java.util.List;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.Result;
import eu.proszowski.democalculator.domain.element.Element;
import eu.proszowski.democalculator.domain.operation.Operation;

public class CalculationTextFormatter {

    private String elementsDelimiter;
    private Mode currentMode;

    public CalculationTextFormatter() {
        this.elementsDelimiter = "\n";
        this.currentMode = Mode.SIMPLE;
    }

    public String format(Calculation calculation) {
        List<Element> numberElements = calculation.getElements();
        List<Operation> operations = calculation.getOperations();

        StringBuilder calculationText = new StringBuilder();

        for (int i = 0; i < numberElements.size(); i++) {
            calculationText.append(numberElements.get(i).asText());
            if (i < operations.size()) {
                calculationText.append(elementsDelimiter);
                calculationText.append(operations.get(i).getSymbol());
                calculationText.append(" ");
            }
        }

        Result result = calculation.getResult();
        if (result != null) {
            calculationText.append("\n= ").append(calculation.getResult().asText());
            if (result.getErrorMessage() != null) {
                return result.getErrorMessage();
            }
        }

        return calculationText.toString();
    }

    public void changeMode() {
        switch (currentMode) {
            case SIMPLE:
                elementsDelimiter = " ";
                currentMode = Mode.ADVANCED;
                break;
            case ADVANCED:
                elementsDelimiter = "\n";
                currentMode = Mode.SIMPLE;
                break;
        }
    }
}
