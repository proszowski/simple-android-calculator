package eu.proszowski.democalculator.command;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.CosinusElement;

public class CosinusCommand extends FunctionCommand {

    @Override
    public Calculation execute(Calculation calculation) {
        super.execute(calculation);
        elements.add(new CosinusElement());
        return new Calculation(elements, operations);
    }
}
