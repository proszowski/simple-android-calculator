package eu.proszowski.democalculator.command;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.Element;
import eu.proszowski.democalculator.domain.element.NumberElement;

public class PercentageCommand extends CalculatorCommand {
    @Override
    public Calculation execute(Calculation calculation) {
        super.execute(calculation);
        Deque<Element> numberElements = new LinkedList<>(this.elements);
        Element element = numberElements.pollLast();

        if (calculation.getResult() != null) {
            return calculation;
        }

        Double elementValue = element.getValue();
        if (elementValue == 0) {
            return calculation;
        }

        String textValueBeforeDot = getTextBeforeDot(elementValue);
        String textValueAfterDot = getTextAfterDot(elementValue);

        if (textValueBeforeDot.length() > 1) {
            String twoLastDigits = textValueBeforeDot.substring(textValueBeforeDot.length() - 2);
            if (textValueAfterDot.equals("0")) {
                textValueAfterDot = twoLastDigits;
            } else {
                textValueAfterDot = twoLastDigits.concat(textValueAfterDot);
            }
            textValueBeforeDot = textValueBeforeDot.substring(0, textValueBeforeDot.length() - 2);
            if (textValueBeforeDot.isEmpty()) {
                textValueBeforeDot = "0";
            }
        } else {
            if (textValueAfterDot.equals("0")) {
                textValueAfterDot = "";
            }
            textValueAfterDot = "0".concat(textValueBeforeDot).concat(textValueAfterDot);
            textValueBeforeDot = "0";
        }

        numberElements.addLast(new NumberElement(textValueBeforeDot + "." + textValueAfterDot));

        return new Calculation(new ArrayList<>(numberElements), operations);
    }

    private String getTextBeforeDot(Double elementValue) {
        String value = String.valueOf(elementValue);
        int dotIndex = value.indexOf(".");
        if (dotIndex == -1) {
            return value;
        } else {
            return value.substring(0, dotIndex).replace(".", "");
        }
    }

    private String getTextAfterDot(Double elementValue) {
        String value = String.valueOf(elementValue);
        int dotIndex = value.indexOf(".");
        if (dotIndex == -1) {
            return "";
        } else {
            return value.substring(dotIndex).replace(".", "");
        }
    }
}
