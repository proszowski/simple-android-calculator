package eu.proszowski.democalculator.command;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.NumberElement;

public class EqualsCommand extends CalculatorCommand {
    @Override
    public Calculation execute(Calculation calculation) {
        super.execute(calculation);
        if (operations.size() == elements.size()) {
            elements.add(new NumberElement());
        }
        Calculation newCalculation = new Calculation(elements, operations);
        newCalculation.calculateResult();
        return newCalculation;
    }
}
