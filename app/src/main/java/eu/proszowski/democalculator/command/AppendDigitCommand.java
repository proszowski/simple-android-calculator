package eu.proszowski.democalculator.command;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.Digit;
import eu.proszowski.democalculator.domain.element.Element;
import eu.proszowski.democalculator.domain.element.NumberElement;

public class AppendDigitCommand extends CalculatorCommand {

    private final Digit digit;

    public AppendDigitCommand(Digit digit) {
        this.digit = digit;
    }

    @Override
    public Calculation execute(Calculation calculation) {
        super.execute(calculation);
        if (calculation.getResult() != null) {
            elements.clear();
            operations.clear();
        }

        if (elements.size() == operations.size()) {
            Element element = new NumberElement(String.valueOf(digit.getValue()));
            elements.add(element);
        } else {
            Element lastElement = elements.getLast();
            lastElement.append(Digit.from(digit.getValue()));
        }

        return new Calculation(elements, operations);
    }
}
