package eu.proszowski.democalculator.command;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.TangesElement;

public class TangesCommand extends FunctionCommand {
    @Override
    public Calculation execute(Calculation calculation) {
        super.execute(calculation);
        elements.add(new TangesElement());
        return new Calculation(elements, operations);
    }
}
