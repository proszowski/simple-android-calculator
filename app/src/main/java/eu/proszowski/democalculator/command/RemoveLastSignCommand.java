package eu.proszowski.democalculator.command;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.Element;
import eu.proszowski.democalculator.domain.element.NumberElement;

public class RemoveLastSignCommand extends CalculatorCommand {

    @Override
    public Calculation execute(Calculation calculation) {
        super.execute(calculation);
        int operationsSize = operations.size();
        int elementsSize = elements.size();
        if (operationsSize == elementsSize && operationsSize != 0) {
            operations.removeLast();
        } else {
            int length = elements.size();
            if (length > 0) {
                Element lastElement = elements.getLast();
                if (lastElement.asText().length() == 1 && operations.size() > 0) {
                    elements.remove(lastElement);
                } else if (isNotANumber(lastElement)) {
                    String textValue = lastElement.asText().replaceAll("[^\\d.-]", "");
                    if (textValue.equals("0")) {
                        elements.remove(lastElement);
                        elements.add(new NumberElement());
                    } else {
                        lastElement.removeLastSign();
                    }
                } else {
                    lastElement.removeLastSign();
                }
            }
        }

        return new Calculation(elements, operations);
    }

    private boolean isNotANumber(Element lastElement) {
        return !(lastElement instanceof NumberElement);
    }

}
