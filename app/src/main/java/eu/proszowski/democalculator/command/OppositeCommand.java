package eu.proszowski.democalculator.command;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.Element;
import eu.proszowski.democalculator.domain.element.OppositeElement;

public class OppositeCommand extends OperationCommand{
    @Override
    public Calculation execute(Calculation calculation) {
        super.execute(calculation);
        Element lastElement = elements.pollLast();
        if(lastElement instanceof OppositeElement){
            OppositeElement oppositeElement = (OppositeElement) lastElement;
            elements.add(oppositeElement.getOppositeElement());
        }else {
            elements.add(new OppositeElement(lastElement));
        }
        return new Calculation(elements, operations);
    }
}
