package eu.proszowski.democalculator.command;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.Element;
import eu.proszowski.democalculator.domain.element.NumberElement;

public class AppendDotCommand extends CalculatorCommand {
    @Override
    public Calculation execute(Calculation calculation) {
        super.execute(calculation);
        if (calculation.getResult() != null) {
            elements.clear();
            operations.clear();
            elements.add(new NumberElement());
        }
        int elementsLength = elements.size();
        Element lastElement = elements.getLast();
        if (operations.size() == elementsLength) {
            Element element = new NumberElement();
            element.appendDot();
            elements.add(element);
        } else {
            lastElement.appendDot();
        }
        return new Calculation(elements, operations);
    }
}
