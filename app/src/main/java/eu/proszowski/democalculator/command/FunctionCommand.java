package eu.proszowski.democalculator.command;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.NumberElement;
import eu.proszowski.democalculator.domain.operation.Operations;

public abstract class FunctionCommand extends CalculatorCommand {

    @Override
    public Calculation execute(Calculation calculation) {
        super.execute(calculation);
        if (elements.size() == 1 && elements.getLast().asText().equals("0")) {
            elements.clear();
        }

        if (calculation.getResult() != null) {
            operations.clear();
            elements.clear();
            elements.add(new NumberElement(calculation.getResult().asText()));
        }

        if (elements.size() > operations.size()) {
            operations.add(Operations.MULTIPLICATION);
        }

        return calculation;
    }
}
