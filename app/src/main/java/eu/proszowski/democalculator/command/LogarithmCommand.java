package eu.proszowski.democalculator.command;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.LogarithmElement;

public class LogarithmCommand extends FunctionCommand {

    @Override
    public Calculation execute(Calculation calculation) {
        super.execute(calculation);
        elements.add(new LogarithmElement());
        return new Calculation(elements, operations);
    }
}
