package eu.proszowski.democalculator.command;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.NumberElement;

public abstract class OperationCommand extends CalculatorCommand {
    @Override
    public Calculation execute(Calculation calculation) {
        super.execute(calculation);
        if (calculation.getResult() != null) {
            operations.clear();
            elements.clear();
            elements.add(new NumberElement(calculation.getResult().asText()));
        }
        if (operations.size() == elements.size()) {
            operations.removeLast();
        }
        return calculation;
    }
}
