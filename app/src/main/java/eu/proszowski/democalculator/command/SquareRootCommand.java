package eu.proszowski.democalculator.command;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.SquareRootElement;

public class SquareRootCommand extends FunctionCommand {
    @Override
    public Calculation execute(Calculation calculation) {
        super.execute(calculation);
        elements.add(new SquareRootElement());
        return new Calculation(elements, operations);
    }
}
