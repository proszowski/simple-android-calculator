package eu.proszowski.democalculator.command;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.NumberElement;
import eu.proszowski.democalculator.domain.operation.Operations;

public class SumCommand extends OperationCommand {
    @Override
    public Calculation execute(Calculation calculation) {
        super.execute(calculation);
        if (calculation.getResult() != null) {
            operations.clear();
            elements.clear();
            elements.add(new NumberElement(calculation.getResult().asText()));
        }

        operations.add(Operations.ADDITION);
        return new Calculation(elements, operations);
    }
}
