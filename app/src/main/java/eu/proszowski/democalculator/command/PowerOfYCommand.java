package eu.proszowski.democalculator.command;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.operation.Operations;

public class PowerOfYCommand extends OperationCommand{
    @Override
    public Calculation execute(Calculation calculation) {
        super.execute(calculation);
        operations.addLast(Operations.POWER_OF_Y);
        return new Calculation(elements, operations);
    }
}
