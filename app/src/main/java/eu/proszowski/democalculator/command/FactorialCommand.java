package eu.proszowski.democalculator.command;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.Element;
import eu.proszowski.democalculator.domain.element.FactorialElement;

public class FactorialCommand extends CalculatorCommand {
    @Override
    public Calculation execute(Calculation calculation) {
        super.execute(calculation);
        Element lastElement = elements.pollLast();
        if(lastElement instanceof FactorialElement){
            elements.addLast(lastElement);
        }else {
            Element newElement = new FactorialElement(lastElement);
            elements.addLast(newElement);
        }
        return new Calculation(elements, operations);
    }
}
