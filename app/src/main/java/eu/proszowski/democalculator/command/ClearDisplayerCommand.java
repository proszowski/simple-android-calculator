package eu.proszowski.democalculator.command;

import eu.proszowski.democalculator.domain.Calculation;

public class ClearDisplayerCommand extends CalculatorCommand {
    @Override
    public Calculation execute(Calculation calculation) {
        return new Calculation();
    }
}
