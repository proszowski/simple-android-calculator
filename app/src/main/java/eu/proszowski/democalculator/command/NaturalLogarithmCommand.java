package eu.proszowski.democalculator.command;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.NaturalLogarithm;

public class NaturalLogarithmCommand extends FunctionCommand {
    @Override
    public Calculation execute(Calculation calculation) {
        super.execute(calculation);
        elements.add(new NaturalLogarithm());
        return new Calculation(elements, operations);
    }
}
