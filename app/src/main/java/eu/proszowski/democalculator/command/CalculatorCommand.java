package eu.proszowski.democalculator.command;

import java.util.Deque;
import java.util.LinkedList;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.Element;
import eu.proszowski.democalculator.domain.operation.Operation;

public abstract class CalculatorCommand {
    protected Deque<Element> elements;
    protected Deque<Operation> operations;

    public Calculation execute(Calculation calculation) {
        elements = new LinkedList<>(calculation.getElements());
        operations = new LinkedList<>(calculation.getOperations());
        return calculation;
    }
}
