package eu.proszowski.democalculator.command;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.operation.Operations;

public class MultiplyCommand extends OperationCommand {

    @Override
    public Calculation execute(Calculation calculation) {
        super.execute(calculation);
        operations.add(Operations.MULTIPLICATION);
        return new Calculation(elements, operations);
    }
}
