package eu.proszowski.democalculator.command;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.SinusElement;

public class SinusCommand extends FunctionCommand {

    @Override
    public Calculation execute(Calculation calculation) {
        super.execute(calculation);
        elements.add(new SinusElement());
        return new Calculation(elements, operations);
    }
}
