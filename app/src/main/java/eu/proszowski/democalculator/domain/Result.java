package eu.proszowski.democalculator.domain;

import java.text.DecimalFormat;
import java.text.Format;
import java.util.Deque;
import java.util.LinkedList;

import eu.proszowski.democalculator.domain.element.Element;
import eu.proszowski.democalculator.domain.element.NumberElement;
import eu.proszowski.democalculator.domain.exception.DomainException;
import eu.proszowski.democalculator.domain.operation.Operation;
import eu.proszowski.democalculator.domain.operation.Operation.Weight;

public class Result {

    private Deque<Element> elements;
    private Deque<Operation> operations;
    private Double result;
    private String errorMessage;

    public Result(Calculation calculation) {
        elements = new LinkedList<>(calculation.getElements());
        operations = new LinkedList<>(calculation.getOperations());
        try {
            result = calculate();
        } catch (DomainException e) {
            result = 0d;
            errorMessage = e.getMessage();
        }
    }

    private Double calculate() {

        if (elements.size() == 0) {
            return 0d;
        }

        if (elements.size() == 1) {
            Element element = elements.poll();
            return element.getValue();
        }

        Deque<Element> elementsDeque = new LinkedList<>();
        Deque<Operation> operationsDeque = new LinkedList<>();

        feedStacks(elementsDeque, operationsDeque);

        while (elementsDeque.size() != 1) {
            Operation nextOperation = operationsDeque.pollLast();
            performOperation(elementsDeque, nextOperation);
        }

        Element result = elementsDeque.poll();
        return result.getValue();
    }

    private void feedStacks(Deque<Element> elementsDeque, Deque<Operation> operationsDeque) {
        while (elements.size() > 0) {
            elementsDeque.addLast(elements.pollFirst());
            Operation lastOperation = operationsDeque.peekLast();
            if (lastOperation != null) {
                if (isHighest(lastOperation.getWeight()) && !operationsDeque.isEmpty()) {
                    performOperation(elementsDeque, lastOperation);
                    operationsDeque.removeLast();
                }
            }
            Operation nextOperation = operations.poll();
            if (nextOperation != null) {
                operationsDeque.addLast(nextOperation);
            }
        }
    }

    private boolean isHighest(Weight weight) {
        int highestValue = Weight.FIRST_LEVEL.getValue();
        for (Operation operation : operations) {
            int currentValue = operation.getWeight().getValue();
            if (currentValue > highestValue) {
                highestValue = currentValue;
            }
        }

        return weight.getValue() >= highestValue;
    }

    private void performOperation(Deque<Element> elementsDeque, Operation nextOperation) {
        Element secondElement = elementsDeque.pollLast();
        Element firstElement = elementsDeque.pollLast();
        Double result = nextOperation.calculate(firstElement, secondElement);
        String resultAsText = String.valueOf(result);
        if (resultAsText.length() > NumberElement.LIMIT) {
            Format format = new DecimalFormat("0.000000000E0");
            resultAsText = format.format(result);
        }
        elementsDeque.addLast(new NumberElement(resultAsText));
    }

    public Double getRaw() {
        return result;
    }

    public String asText() {
        String text = String.valueOf(result).toLowerCase();
        if (text.endsWith(".0")) {
            return text.substring(0, text.length() - 2).toLowerCase();
        } else {
            return text;
        }
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
