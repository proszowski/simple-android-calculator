package eu.proszowski.democalculator.domain.exception;

public class InvalidValueForFactorialOperationException extends DomainException {
    private String message;

    public InvalidValueForFactorialOperationException() {
        this.message = "Cannot calculate factorial of decimal numbers";
    }

    @Override
    public String getMessage() {
        return message;
    }
}
