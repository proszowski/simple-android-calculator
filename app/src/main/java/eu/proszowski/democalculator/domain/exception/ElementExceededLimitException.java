package eu.proszowski.democalculator.domain.exception;

public class ElementExceededLimitException extends DomainException {
    private ElementExceededLimitException() {
        super();
    }

    private ElementExceededLimitException(String message) {
        super(message);
    }

    public static ElementExceededLimitException of(int length, int limit) {
        String message = String.format("Element of calculation can't exceed limit. Actual length: %s, limit: %s",
                length + 1, limit);
        return new ElementExceededLimitException(message);
    }
}
