package eu.proszowski.democalculator.domain.exception;

public class DigitDoesNotExistException extends RuntimeException {

    private String message;

    public DigitDoesNotExistException() {
        this.message = "Digit does not exist. Available digits are integers in range <0,9>";
    }

    @Override
    public String getMessage() {
        return message;
    }
}
