package eu.proszowski.democalculator.domain.operation;

import eu.proszowski.democalculator.domain.element.Element;

class PowerOfY extends Operation {
    @Override
    public String getSymbol() {
        return "^";
    }

    @Override
    public Weight getWeight() {
        return Weight.THIRD_LEVEL;
    }

    @Override
    public double calculate(Element firstElement, Element secondElement) {
        super.calculate(firstElement, secondElement);
        return Math.pow(firstValue, secondValue);
    }
}
