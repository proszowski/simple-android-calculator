package eu.proszowski.democalculator.domain.operation;

import eu.proszowski.democalculator.domain.element.Element;

public abstract class Operation {
    protected Double firstValue;
    protected Double secondValue;

    public abstract String getSymbol();

    public double calculate(Element firstElement, Element secondElement) {
        firstValue = firstElement != null ? firstElement.getValue() : 0d;
        secondValue = secondElement != null ? secondElement.getValue() : 0d;
        return 0;
    }

    public abstract Operation.Weight getWeight();

    public enum Weight {
        FIRST_LEVEL(0),
        SECOND_LEVEL(1),
        THIRD_LEVEL(2);

        private Integer value;

        Weight(Integer value) {
            this.value = value;
        }

        public Integer getValue() {
            return value;
        }
    }
}
