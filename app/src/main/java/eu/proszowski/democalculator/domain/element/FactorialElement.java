package eu.proszowski.democalculator.domain.element;

import eu.proszowski.democalculator.domain.exception.InvalidValueForFactorialOperationException;

public class FactorialElement extends FunctionElement {

    private Element element;

    public FactorialElement(Element element) {
        super();
        this.element = element;
    }

    public FactorialElement() {
        super();
        this.element = new NumberElement();
    }

    @Override
    public Double getValue() {
        return factorOf(element.getValue());
    }

    private Double factorOf(Double value) {
        if (value.longValue() < value) {
            throw new InvalidValueForFactorialOperationException();
        }

        double result = 1d;
        int infinityBorder = 171;

        if (value.longValue() > infinityBorder) {
            value = (double) infinityBorder;
        }

        for (int i = 2; i <= value; i++) {
            result *= i;
        }

        return result;
    }

    @Override
    public String asText() {
        return element.asText() + "!";
    }

}
