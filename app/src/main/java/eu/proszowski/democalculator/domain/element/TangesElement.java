package eu.proszowski.democalculator.domain.element;

import eu.proszowski.democalculator.domain.Result;

public class TangesElement extends FunctionElement {

    public TangesElement() {
        super();
    }

    @Override
    public Double getValue() {
        Result result = new Result(calculation);
        return Math.tan(result.getRaw());
    }

    @Override
    public String asText() {
        Result result = new Result(calculation);
        return "tan(" + result.asText() + ")";
    }
}
