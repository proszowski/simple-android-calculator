package eu.proszowski.democalculator.domain.operation;

import eu.proszowski.democalculator.domain.element.Element;
import eu.proszowski.democalculator.domain.exception.DivisionByZeroException;

class Division extends Operation {
    @Override
    public String getSymbol() {
        return "÷";
    }

    @Override
    public double calculate(Element firstElement, Element secondElement) {
        super.calculate(firstElement, secondElement);
        if (secondValue == 0) {
            throw new DivisionByZeroException();
        }
        return firstValue / secondValue;
    }

    @Override
    public Weight getWeight() {
        return Weight.SECOND_LEVEL;
    }
}
