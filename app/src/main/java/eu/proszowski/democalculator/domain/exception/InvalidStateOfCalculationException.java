package eu.proszowski.democalculator.domain.exception;

public class InvalidStateOfCalculationException extends DomainException {

    public InvalidStateOfCalculationException(String message) {
        super(message);
    }

    public static InvalidStateOfCalculationException incorrectSizes(int elementsSize, int operationsSize) {
        return new InvalidStateOfCalculationException(String.format("There can't be more operations than elements and" +
                " elements size can be bigger only by one than size of operations. elements size: %s, Operations " +
                "size: %s", elementsSize, operationsSize));
    }
}
