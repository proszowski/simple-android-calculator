package eu.proszowski.democalculator.domain.element;

import java.util.Objects;

import eu.proszowski.democalculator.domain.Digit;
import eu.proszowski.democalculator.domain.exception.ElementExceededLimitException;

public class NumberElement implements Element {

    public static final Integer LIMIT = 15;
    private String value;

    public NumberElement() {
        this("0");
    }

    public NumberElement(String value) {
        validate(value);
        this.value = withoutCommas(value);
    }

    private String withoutCommas(String value) {
        return value.replace(",", "");
    }

    private void validate(String value) {
        if (value.length() > LIMIT) {
            throw ElementExceededLimitException.of(value.length(), LIMIT);
        }
    }

    public void appendDot() {
        if (!value.contains(".")) {
            value = value.concat(".");
        }
    }

    public void append(Digit digit) {
        int length = value.length();
        if (value.length() == LIMIT) {
            throw ElementExceededLimitException.of(length, LIMIT);
        }

        if (value.length() == 1 && Double.valueOf(value) == 0) {
            value = String.valueOf(digit.getValue());
            return;
        }

        value = value.concat(String.valueOf(digit.getValue()));
    }

    public void removeLastSign() {
        int length = value.length();
        if (length > 1) {
            value = value.substring(0, length - 1);
        } else {
            value = "0";
        }
    }

    public Double getValue() {
        try {
           return Double.parseDouble(value);
        }catch (NumberFormatException e){
            return 0d;
        }
    }

    public String asText() {
        if (!value.contains(".")) {
            return getTextBeforeDot();
        }
        return value;
    }

    private String getTextBeforeDot() {
        int dotIndex = value.indexOf(".");
        if (dotIndex == -1) {
            return value;
        } else {
            return value.substring(0, dotIndex).replace(".", "");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NumberElement)) return false;
        NumberElement numberElement = (NumberElement) o;
        return Objects.equals(value, numberElement.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
