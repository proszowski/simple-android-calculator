package eu.proszowski.democalculator.domain.element;

import eu.proszowski.democalculator.domain.Result;

public class CosinusElement extends FunctionElement {

    public CosinusElement() {
        super();
    }

    @Override
    public Double getValue() {
        Result result = new Result(calculation);
        return Math.cos(result.getRaw());
    }

    @Override
    public String asText() {
        Result result = new Result(calculation);
        return "cos(" + result.asText() + ")";
    }
}
