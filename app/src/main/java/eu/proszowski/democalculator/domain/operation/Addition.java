package eu.proszowski.democalculator.domain.operation;

import eu.proszowski.democalculator.domain.element.Element;

public class Addition extends Operation {
    @Override
    public String getSymbol() {
        return "+";
    }

    @Override
    public double calculate(Element firstElement, Element secondElement) {
        super.calculate(firstElement, secondElement);
        return firstValue + secondValue;
    }

    @Override
    public Weight getWeight() {
        return Weight.FIRST_LEVEL;
    }

}
