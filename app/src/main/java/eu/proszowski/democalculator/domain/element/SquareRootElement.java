package eu.proszowski.democalculator.domain.element;

import eu.proszowski.democalculator.domain.Result;

public class SquareRootElement extends FunctionElement {
    @Override
    public Double getValue() {
        Result result = new Result(calculation);
        return Math.sqrt(result.getRaw());
    }

    @Override
    public String asText() {
        Result result = new Result(calculation);
        return "sqrt(" + result.asText() + ")";
    }
}
