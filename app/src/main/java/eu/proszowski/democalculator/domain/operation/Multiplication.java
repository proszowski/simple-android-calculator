package eu.proszowski.democalculator.domain.operation;

import eu.proszowski.democalculator.domain.element.Element;

class Multiplication extends Operation {
    @Override
    public String getSymbol() {
        return "×";
    }

    @Override
    public double calculate(Element firstElement, Element secondElement) {
        super.calculate(firstElement, secondElement);
        return firstValue * secondValue;
    }

    @Override
    public Weight getWeight() {
        return Weight.SECOND_LEVEL;
    }
}
