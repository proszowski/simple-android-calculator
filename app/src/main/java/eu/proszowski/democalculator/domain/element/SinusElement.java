package eu.proszowski.democalculator.domain.element;

import eu.proszowski.democalculator.domain.Result;

public class SinusElement extends FunctionElement {

    public SinusElement() {
        super();
    }

    @Override
    public Double getValue() {
        Result result = new Result(calculation);
        return Math.sin(result.getRaw());
    }

    @Override
    public String asText() {
        Result result = new Result(calculation);
        return "sin(" + result.asText() + ")";
    }
}
