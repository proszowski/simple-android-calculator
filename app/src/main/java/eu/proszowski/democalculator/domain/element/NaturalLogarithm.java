package eu.proszowski.democalculator.domain.element;

import eu.proszowski.democalculator.domain.Result;

public class NaturalLogarithm extends FunctionElement {

    public NaturalLogarithm() {
        super();
    }

    @Override
    public Double getValue() {
        Result result = new Result(calculation);
        return Math.log(result.getRaw());
    }

    @Override
    public String asText() {
        Result result = new Result(calculation);
        return "ln(" + result.asText() + ")";
    }
}
