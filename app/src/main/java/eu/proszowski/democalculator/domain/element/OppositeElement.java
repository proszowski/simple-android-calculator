package eu.proszowski.democalculator.domain.element;

import eu.proszowski.democalculator.domain.Digit;

public class OppositeElement implements Element {

    private Element element;

    public OppositeElement(Element element) {
        this.element = element;
    }

    @Override
    public Double getValue() {
        return -element.getValue();
    }

    @Override
    public String asText() {
        if(element.getValue() >= 0){
            return "(-" + element.asText() + ")";
        }else {
            return element.asText().replace("-", "");
        }
    }

    @Override
    public void append(Digit digit) {
        element.append(digit);
    }

    @Override
    public void appendDot() {
        element.appendDot();
    }

    @Override
    public void removeLastSign() {
        element.removeLastSign();
    }

    public Element getOppositeElement(){
        return element;
    }
}
