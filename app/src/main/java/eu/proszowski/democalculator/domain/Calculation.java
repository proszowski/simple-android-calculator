package eu.proszowski.democalculator.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import eu.proszowski.democalculator.domain.element.Element;
import eu.proszowski.democalculator.domain.element.NumberElement;
import eu.proszowski.democalculator.domain.exception.InvalidStateOfCalculationException;
import eu.proszowski.democalculator.domain.operation.Operation;

public class Calculation {
    private final List<Element> elements;
    private final List<Operation> operations;
    private Result result;

    public Calculation() {
        this(Collections.emptyList(), Collections.emptyList());
    }

    public Calculation(Collection<Element> elements, Collection<Operation> operations) {
        validate(elements, operations);
        if (elements.isEmpty()) {
            this.elements = Collections.singletonList(new NumberElement());
        } else {
            this.elements = new ArrayList<>(elements);
        }
        this.operations = new ArrayList<>(operations);
    }

    public void calculateResult() {
        if (result == null) {
            result = new Result(this);
        }
    }

    public Result getResult() {
        return result;
    }

    private void validate(Collection<Element> elements, Collection<Operation> operations) {
        if (elements.size() < operations.size() || elements.size() - operations.size() > 1) {
            throw InvalidStateOfCalculationException.incorrectSizes(elements.size(), operations.size());
        }
    }

    public List<Element> getElements() {
        return elements;
    }

    public List<Operation> getOperations() {
        return operations;
    }

}
