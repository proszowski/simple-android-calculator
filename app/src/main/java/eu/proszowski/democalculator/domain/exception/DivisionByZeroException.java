package eu.proszowski.democalculator.domain.exception;

public class DivisionByZeroException extends DomainException {
    @Override
    public String getMessage() {
        return "Can't divide by zero";
    }
}
