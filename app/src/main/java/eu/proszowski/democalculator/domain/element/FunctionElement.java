package eu.proszowski.democalculator.domain.element;

import eu.proszowski.democalculator.command.AppendDigitCommand;
import eu.proszowski.democalculator.command.AppendDotCommand;
import eu.proszowski.democalculator.command.RemoveLastSignCommand;
import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.Digit;

public abstract class FunctionElement implements Element {

    protected Calculation calculation;

    public FunctionElement() {
        this.calculation = new Calculation();
    }

    @Override
    public void append(Digit digit) {
        AppendDigitCommand appendDigitCommand = new AppendDigitCommand(digit);
        calculation = appendDigitCommand.execute(calculation);
    }

    @Override
    public void appendDot() {
        AppendDotCommand appendDotCommand = new AppendDotCommand();
        calculation = appendDotCommand.execute(calculation);
    }

    @Override
    public void removeLastSign() {
        RemoveLastSignCommand removeLastSignCommand = new RemoveLastSignCommand();
        calculation = removeLastSignCommand.execute(calculation);
    }
}
