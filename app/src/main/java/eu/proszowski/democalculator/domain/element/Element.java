package eu.proszowski.democalculator.domain.element;

import eu.proszowski.democalculator.domain.Digit;

public interface Element {
    Double getValue();

    String asText();

    void append(Digit digit);

    void appendDot();

    void removeLastSign();
}
