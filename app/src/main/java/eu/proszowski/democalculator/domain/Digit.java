package eu.proszowski.democalculator.domain;

import java.util.EnumSet;
import java.util.Iterator;

import eu.proszowski.democalculator.domain.exception.DigitDoesNotExistException;

public enum Digit {
    ONE(1),
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5),
    SIX(6),
    SEVEN(7),
    EIGHT(8),
    NINE(9),
    ZERO(0);

    private Integer value;

    Digit(int value) {
        this.value = value;
    }

    public static Digit from(Integer value) {
        EnumSet<Digit> digits = EnumSet.allOf(Digit.class);
        Iterator<Digit> it = digits.iterator();
        while (it.hasNext()) {
            Digit digit = it.next();
            if (digit.getValue().equals(value)) {
                return digit;
            }
        }

        throw new DigitDoesNotExistException();
    }

    public Integer getValue() {
        return value;
    }
}
