package eu.proszowski.democalculator.domain.operation;

public final class Operations {
    public static final Operation ADDITION = new Addition();
    public static final Operation SUBTRACTION = new Subtraction();
    public static final Operation MULTIPLICATION = new Multiplication();
    public static final Operation DIVISION = new Division();
    public static final Operation POWER_OF_Y = new PowerOfY();
}
