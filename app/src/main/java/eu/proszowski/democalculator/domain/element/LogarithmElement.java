package eu.proszowski.democalculator.domain.element;

import eu.proszowski.democalculator.domain.Result;

public class LogarithmElement extends FunctionElement {

    public LogarithmElement() {
        super();
    }

    @Override
    public Double getValue() {
        Result result = new Result(calculation);
        return Math.log10(result.getRaw());
    }

    @Override
    public String asText() {
        Result result = new Result(calculation);
        return "log(" + result.asText() + ")";
    }
}
