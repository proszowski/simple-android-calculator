package eu.proszowski.democalculator;

import eu.proszowski.democalculator.command.CalculatorCommand;
import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.exception.DomainException;

public class CalculatorFacade {

    private Calculation calculation;

    public CalculatorFacade() {
        this.calculation = new Calculation();
    }

    public void applyCommand(CalculatorCommand command) {
        try {
            calculation = command.execute(calculation);
        } catch (DomainException e) {
            System.out.println(e.getMessage());
        }
    }

    public String getDisplayerText(CalculationTextFormatter calculationTextFormatter) {
        return calculationTextFormatter.format(calculation);
    }
}
