package eu.proszowski.democalculator.configuration;

import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;

@Component(modules = {AndroidInjectionModule.class, CalculatorApplicationModule.class})
public interface CalculatorComponent extends AndroidInjector<CalculatorApplication> {
}
