package eu.proszowski.democalculator.configuration;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import eu.proszowski.democalculator.CalculationTextFormatter;
import eu.proszowski.democalculator.CalculatorFacade;
import eu.proszowski.democalculator.EventBus;
import eu.proszowski.democalculator.ui.AboutActivity;
import eu.proszowski.democalculator.ui.AdvancedLeftSideButtonsFragment;
import eu.proszowski.democalculator.ui.AdvancedModeActivity;
import eu.proszowski.democalculator.ui.AdvancedUpSideButtonsFragment;
import eu.proszowski.democalculator.ui.DisplayerFragment;
import eu.proszowski.democalculator.ui.NavbarFragment;
import eu.proszowski.democalculator.ui.SimpleModeActivity;
import eu.proszowski.democalculator.ui.SimpleModeButtonsFragment;

@Module
public abstract class CalculatorApplicationModule {
    private final static CalculatorFacade calculatorFacade = new CalculatorFacade();
    private final static EventBus eventBus = new EventBus();
    private final static CalculationTextFormatter calculationTextFormatter = new CalculationTextFormatter();

    @Provides
    static CalculatorFacade provideCalculatorFacade() {
        return calculatorFacade;
    }

    @Provides
    static EventBus provideEventBus() {
        return eventBus;
    }

    @Provides
    static CalculationTextFormatter provideCalculationTextFormatter() {
        return calculationTextFormatter;
    }

    @ContributesAndroidInjector()
    abstract SimpleModeActivity simpleModeActivityContributesAndroidInjector();

    @ContributesAndroidInjector
    abstract AdvancedModeActivity advancedModeActivityContributesAndroidInjector();

    @ContributesAndroidInjector
    abstract AboutActivity aboutActivityContributesAndroidInjector();

    @ContributesAndroidInjector
    abstract DisplayerFragment displayerFragmentContributesAndroidInjector();

    @ContributesAndroidInjector
    abstract SimpleModeButtonsFragment simpleModeButtonsFragmentContributesAndroidInjector();

    @ContributesAndroidInjector
    abstract AdvancedLeftSideButtonsFragment advancedLeftSideButtonsFragmentContributesAndroidInjector();

    @ContributesAndroidInjector
    abstract AdvancedUpSideButtonsFragment advancedUpSideButtonsFragmentContributesAndroidInjector();

    @ContributesAndroidInjector
    abstract NavbarFragment navbarFragmentContributesAndroidInjector();
}
