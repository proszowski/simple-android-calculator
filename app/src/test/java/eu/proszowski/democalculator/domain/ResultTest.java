package eu.proszowski.democalculator.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.Collections;

import eu.proszowski.democalculator.domain.element.NumberElement;
import eu.proszowski.democalculator.domain.operation.Operations;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ResultTest {

    @Test
    void should_cut_end_of_double_value_if_it_is_zero() {
        //given
        Calculation calculation = new Calculation(Arrays.asList(new NumberElement("1"), new NumberElement("2")),
                Collections.singletonList(Operations.SUBTRACTION));
        Result result = new Result(calculation);
        //when
        String text = result.asText();
        //then
        assertEquals("-1", text);
    }

    @ParameterizedTest
    @CsvSource({"1, 2, 3",
            "3.42, 2.42, 5.84"})
    void should_return_result_as_text(String first, String second, String expected) {
        //given
        Calculation calculation = new Calculation(Arrays.asList(new NumberElement(first), new NumberElement(second)),
                Collections.singletonList(Operations.ADDITION));
        Result result = new Result(calculation);
        //when
        String text = result.asText();
        //then
        assertEquals(expected, text);
    }

    @Test
    void should_consider_order_of_operations() {
        //given
        Calculation calculation = new Calculation(Arrays.asList(new NumberElement("5"), new NumberElement("3"),
                new NumberElement("2"),
                new NumberElement("7")),
                Arrays.asList(Operations.ADDITION, Operations.MULTIPLICATION, Operations.SUBTRACTION));
        Result result = new Result(calculation);
        //when
        Double raw = result.getRaw();
        //then
        assertEquals(5 + 3 * 2 - 7, raw);
    }

}