package eu.proszowski.democalculator.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.List;

import eu.proszowski.democalculator.domain.element.Element;
import eu.proszowski.democalculator.domain.exception.InvalidStateOfCalculationException;
import eu.proszowski.democalculator.domain.operation.Operation;

import static eu.proszowski.democalculator.TestUtils.givenOneElement;
import static eu.proszowski.democalculator.TestUtils.givenOneOperation;
import static eu.proszowski.democalculator.TestUtils.givenThreeElements;
import static eu.proszowski.democalculator.TestUtils.givenTwoOperations;

class CalculationTest {

    @Test
    void should_create_calculation_when_valid_set_of_elements_and_operations_provided() {
        //given
        final List<Element> numberElements = givenOneElement();
        final List<Operation> operations = givenOneOperation();
        //when
        Executable createCalculation = () -> new Calculation(numberElements, operations);
        //then
        Assertions.assertDoesNotThrow(createCalculation);
    }

    @Test
    void should_throw_exception_when_more_operations_than_elements() {
        //given
        final List<Element> numberElements = givenOneElement();
        final List<Operation> operations = givenTwoOperations();
        //when
        Executable createCalculation = () -> new Calculation(numberElements, operations);
        //then
        Assertions.assertThrows(InvalidStateOfCalculationException.class, createCalculation);
    }

    @Test
    void should_throw_exception_when_difference_between_size_of_elements_and_operations_is_bigger_than_one() {
        //given
        final List<Element> numberElements = givenThreeElements();
        final List<Operation> operations = givenOneOperation();
        //when
        Executable createCalculation = () -> new Calculation(numberElements, operations);
        //then
        Assertions.assertThrows(InvalidStateOfCalculationException.class, createCalculation);
    }
}