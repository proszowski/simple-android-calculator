package eu.proszowski.democalculator.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import eu.proszowski.democalculator.domain.element.NumberElement;
import eu.proszowski.democalculator.domain.exception.ElementExceededLimitException;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class NumberElementTest {

    @Test
    void should_throw_exception_when_value_exceed_limit() {
        //given
        String numberBiggerThanLimit = "123456789123456789";
        //when
        Executable createElement = () -> new NumberElement(numberBiggerThanLimit);
        //then
        assertThrows(ElementExceededLimitException.class, createElement);
    }

    @Test
    void should_create_element() {
        //given
        String numberBelowLimit = "123456789126";
        //when
        Executable createElement = () -> new NumberElement(numberBelowLimit);
        //then
        assertDoesNotThrow(createElement);
    }

    @Test
    void should_have_only_one_digit_when_only_zero_in_element() {
        //given
        Digit digitToAppend = Digit.FOUR;
        NumberElement numberElement = new NumberElement();
        //when
        numberElement.append(digitToAppend);
        //then
        assertEquals(digitToAppend.getValue(), numberElement.getValue().intValue());
        assertEquals(1, numberElement.asText().length());
    }

    @Test
    void should_have_two_digits_after_append() {
        //given
        Digit digitToAppend = Digit.EIGHT;
        NumberElement numberElement = new NumberElement("1.3");
        //when
        numberElement.append(digitToAppend);
        //then
        assertEquals(1.38, numberElement.getValue());
        assertEquals(4, numberElement.asText().length());
    }

    @Test
    void should_add_digit_after_dot() {
        //given
        Digit digitToAppend = Digit.THREE;
        NumberElement numberElement = new NumberElement("7");
        //when
        numberElement.appendDot();
        numberElement.append(digitToAppend);
        //then
        assertEquals(7.3, numberElement.getValue());
        assertEquals(3, numberElement.asText().length());

    }

    @Test
    void should_have_four_signs_when_append_digit_after_zero_after_dot() {
        //given
        Digit digitToAppend = Digit.FIVE;
        NumberElement numberElement = new NumberElement("7");
        numberElement.appendDot();
        numberElement.append(Digit.ZERO);
        //when
        numberElement.append(digitToAppend);
        //then
        assertEquals(7.05, numberElement.getValue());
        assertEquals(4, numberElement.asText().length());
    }

    @Test
    void should_throw_exception_when_limit_exceeded_during_append_operation() {
        //given
        Digit digitToAppend = Digit.SEVEN;
        NumberElement numberElement = new NumberElement("123456789123456");
        //when
        Executable append = () -> numberElement.append(digitToAppend);
        //then
        assertThrows(ElementExceededLimitException.class, append);
    }

    @Test
    void should_not_exceed_limit_when_appending_zeros_to_zero_before_dot() {
        //given
        Digit digitToAppend = Digit.ZERO;
        NumberElement numberElement = new NumberElement();
        //when
        Executable append = () -> {
            for (int i = 0; i < 20; i++) {
                numberElement.append(digitToAppend);
            }
        };
        //then
        assertDoesNotThrow(append);
    }

    @Test
    void should_return_zero_when_number_contains_other_signs_than_digits_or_minus(){
        //given
        NumberElement numberElement = new NumberElement("infinity");
        //when
        double value = numberElement.getValue();
        //then
        assertEquals(0, value);
    }

}