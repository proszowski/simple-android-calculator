package eu.proszowski.democalculator;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import eu.proszowski.democalculator.command.AppendDigitCommand;
import eu.proszowski.democalculator.command.AppendDotCommand;
import eu.proszowski.democalculator.command.RemoveLastSignCommand;
import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.Digit;
import eu.proszowski.democalculator.domain.element.NumberElement;
import eu.proszowski.democalculator.domain.operation.Operations;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CalculationTextFormatterTest {

    CalculationTextFormatter calculationTextFormatter = new CalculationTextFormatter();

    @Test
    void should_display_zero_when_calculation_empty() {
        //given
        Calculation calculation = new Calculation();
        //when
        String text = calculationTextFormatter.format(calculation);
        //then
        assertEquals("0", text);
    }

    @Test
    void should_display_number_with_dot() {
        //given
        Calculation calculation = new Calculation(Collections.singletonList(new NumberElement("123.433")),
                Collections.emptyList());
        //when
        String text = calculationTextFormatter.format(calculation);
        //then
        assertEquals("123.433", text);
    }

    @Test
    void should_display_digit_with_dot_when_removing_last_zero_after_dot() {
        //given
        Calculation calculation = new Calculation(Collections.singletonList(new NumberElement("654")),
                Collections.emptyList());
        calculation = new AppendDotCommand().execute(calculation);
        calculation = new AppendDigitCommand(Digit.ZERO).execute(calculation);
        calculation = new RemoveLastSignCommand().execute(calculation);
        //when
        String text = calculationTextFormatter.format(calculation);
        //then
        assertEquals("654.", text);
    }

    @Test
    void should_display_many_zeros_after_dot() {
        //given
        Calculation calculation = new Calculation(Collections.singletonList(new NumberElement("352")),
                Collections.emptyList());
        calculation = new AppendDotCommand().execute(calculation);
        calculation = appendTenZeros(calculation);
        //when
        String text = calculationTextFormatter.format(calculation);
        //then
        assertEquals("352.0000000000", text);
    }

    @Test
    void should_display_zeros_when_last_digit_is_appended_at_the_end() {
        //given
        Calculation calculation = new Calculation(Collections.singletonList(new NumberElement("19")),
                Collections.emptyList());
        calculation = new AppendDotCommand().execute(calculation);
        calculation = appendTenZeros(calculation);
        calculation = new AppendDigitCommand(Digit.EIGHT).execute(calculation);
        //when
        String text = calculationTextFormatter.format(calculation);
        //then
        assertEquals("19.00000000008", text);
    }

    @Test
    void should_display_operations() {
        //given
        Calculation calculation = new Calculation(Arrays.asList(new NumberElement(), new NumberElement("43.5")),
                Collections.singletonList(Operations.ADDITION));
        //when
        String text = calculationTextFormatter.format(calculation);
        //then
        assertEquals("0\n+ 43.5", text);

    }

    @Test
    void should_display_error_message_when_division_by_zero() {
        //given
        Calculation calculation = new Calculation(Arrays.asList(new NumberElement("1"), new NumberElement()),
                Collections.singletonList(Operations.DIVISION));
        //when
        calculation.calculateResult();
        String text = calculationTextFormatter.format(calculation);
        //then
        assertEquals("Can't divide by zero", text);
    }

    private Calculation appendTenZeros(Calculation calculation) {
        for (int i = 0; i < 10; i++) {
            calculation = new AppendDigitCommand(Digit.ZERO).execute(calculation);
        }
        return calculation;
    }
}