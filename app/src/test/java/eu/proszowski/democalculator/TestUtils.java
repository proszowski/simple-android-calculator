package eu.proszowski.democalculator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import eu.proszowski.democalculator.domain.element.Element;
import eu.proszowski.democalculator.domain.element.NumberElement;
import eu.proszowski.democalculator.domain.operation.Operation;
import eu.proszowski.democalculator.domain.operation.Operations;

public class TestUtils {

    public static List<Element> givenThreeElements() {
        return Arrays.asList(new NumberElement("13.2"), new NumberElement("123"), new NumberElement("738.123"));
    }

    public static List<Operation> givenTwoOperations() {
        return Arrays.asList(Operations.ADDITION, Operations.SUBTRACTION);
    }

    public static List<Element> givenOneElement() {
        return Collections.singletonList(new NumberElement("2342"));
    }

    public static List<Operation> givenOneOperation() {
        return Collections.singletonList(Operations.ADDITION);
    }

    public static Element getLastElement(List<Element> numberElements) {
        int length = numberElements.size();
        return numberElements.get(length - 1);
    }
}
