package eu.proszowski.democalculator;

import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EventBusTest {

    @Test
    void should_subscribe_for_an_event_emit_and_execute_properly() {
        //given
        EventBus eventBus = new EventBus();
        Date date = new Date();
        //when
        eventBus.subscribe(Event.CALCULATION_CHANGED, () -> date.setTime(2));
        eventBus.emit(Event.CALCULATION_CHANGED);
        //then
        assertEquals(2, date.getTime());
    }

}