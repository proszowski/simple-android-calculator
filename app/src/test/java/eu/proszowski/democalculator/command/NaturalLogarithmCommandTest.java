package eu.proszowski.democalculator.command;

import org.junit.jupiter.api.Test;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.Digit;
import eu.proszowski.democalculator.domain.Result;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NaturalLogarithmCommandTest {


    @Test
    void should_properly_calculate_natural_logarithm() {
        //given
        Calculation calculation = new Calculation();
        NaturalLogarithmCommand logarithmCommand = new NaturalLogarithmCommand();
        calculation = logarithmCommand.execute(calculation);
        AppendDigitCommand appendDigitCommand = new AppendDigitCommand(Digit.TWO);
        calculation = appendDigitCommand.execute(calculation);
        //when
        calculation.calculateResult();
        //then
        Result result = calculation.getResult();
        assertEquals(Math.log(2), result.getRaw());
    }
}