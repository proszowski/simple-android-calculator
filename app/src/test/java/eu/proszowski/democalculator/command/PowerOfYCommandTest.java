package eu.proszowski.democalculator.command;

import org.junit.jupiter.api.Test;

import java.util.Collections;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.Digit;
import eu.proszowski.democalculator.domain.element.NumberElement;

import static org.junit.jupiter.api.Assertions.*;

class PowerOfYCommandTest {

    @Test
    void should_calculate_power_of_y(){
        //given
        Calculation calculation = new Calculation(Collections.singletonList(new NumberElement("25")), Collections.emptyList());
        PowerOfYCommand powerOfYCommand = new PowerOfYCommand();
        AppendDigitCommand appendDigitCommand = new AppendDigitCommand(Digit.THREE);
        //when
        calculation = powerOfYCommand.execute(calculation);
        calculation = appendDigitCommand.execute(calculation);
        calculation.calculateResult();
        //then
        assertEquals(Math.pow(25, 3), calculation.getResult().getRaw());
    }

}