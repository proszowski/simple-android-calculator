package eu.proszowski.democalculator.command;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.Collections;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.NumberElement;
import eu.proszowski.democalculator.domain.operation.Operations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SumCommandTest {

    @Test
    void should_add_operation_to_calculation() {
        //given
        Calculation calculation = new Calculation(Collections.singletonList(new NumberElement("2")),
                Collections.emptyList());
        CalculatorCommand sumCommand = new SumCommand();
        //when
        calculation = sumCommand.execute(calculation);
        //then
        assertTrue(calculation.getOperations().contains(Operations.ADDITION));
    }

    @ParameterizedTest
    @CsvSource({"2, 2, 4",
            "-9, 29, 20",
            "32.31312, 323.242, 355.55512"})
    void should_add_two_numbers(String a, String b, Double expected) {
        //given
        Calculation calculation = new Calculation(Arrays.asList(new NumberElement(a), new NumberElement(b)),
                Collections.singletonList(Operations.ADDITION));
        CalculatorCommand sumCommand = new SumCommand();
        //when
        calculation = sumCommand.execute(calculation);
        calculation.calculateResult();
        //then
        assertEquals(expected, calculation.getResult().getRaw());
    }

}