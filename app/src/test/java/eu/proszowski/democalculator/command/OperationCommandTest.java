package eu.proszowski.democalculator.command;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.NumberElement;
import eu.proszowski.democalculator.domain.operation.Operations;

import static org.junit.jupiter.api.Assertions.*;

class OperationCommandTest {

    @Test
    void should_replace_last_operation_when_amount_of_operations_and_elements_is_the_same(){
        //given
        Calculation calculation = new Calculation(Collections.singletonList(new NumberElement()),
                Collections.singletonList(Operations.MULTIPLICATION));
        SumCommand sumCommand = new SumCommand();
        //when
        calculation = sumCommand.execute(calculation);
        //then
        assertEquals(1, calculation.getOperations().size());
        assertTrue(calculation.getOperations().contains(Operations.ADDITION));
    }

}