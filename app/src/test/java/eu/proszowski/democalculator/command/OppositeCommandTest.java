package eu.proszowski.democalculator.command;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Collections;
import java.util.List;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.Result;
import eu.proszowski.democalculator.domain.element.Element;
import eu.proszowski.democalculator.domain.element.NumberElement;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OppositeCommandTest {

    @ParameterizedTest
    @CsvSource({"3, -3", "-10, 10"})
    void should_change_last_element_into_opposition(String value, String expected) {
        //given
        Calculation calculation = new Calculation(Collections.singleton(new NumberElement(value)),
                Collections.emptyList());
        OppositeCommand oppositeCommand = new OppositeCommand();
        //when
        calculation = oppositeCommand.execute(calculation);
        calculation.calculateResult();
        //then
        Result result = calculation.getResult();
        assertEquals(expected, result.asText());
    }

    @Test
    void should_remove_minus_sign_before_number() {
        //given
        Calculation calculation = new Calculation(Collections.singleton(new NumberElement("-19")),
                Collections.emptyList());
        OppositeCommand oppositeCommand = new OppositeCommand();
        //when
        calculation = oppositeCommand.execute(calculation);
        //then
        List<Element> elements = calculation.getElements();
        Element lastElement = elements.get(elements.size() - 1);
        assertEquals("19", lastElement.asText());
    }

}