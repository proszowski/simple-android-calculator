package eu.proszowski.democalculator.command;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.Element;
import eu.proszowski.democalculator.domain.element.NumberElement;
import eu.proszowski.democalculator.domain.operation.Operations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PercentageCommandTest {

    @Test
    void should_not_append_operation_to_calculation() {
        //given
        Calculation calculation = new Calculation(Collections.singletonList(new NumberElement("3")),
                Collections.emptyList());
        CalculatorCommand percentageCommand = new PercentageCommand();
        //when
        calculation = percentageCommand.execute(calculation);
        //then
        assertTrue(calculation.getOperations().isEmpty());
    }

    @ParameterizedTest(name = "calculatePercentage of {0} should be {1}")
    @CsvSource({"324, 3.24",
            "0, 0",
            "12.23, 0.1223",
            "4, 0.04",
            "0.234, 0.00234"
    })
    void should_get_percentage_of_given_number_as__a_result(String value, double expected) {
        //given
        Calculation calculation = new Calculation(Collections.singletonList(new NumberElement(value)),
                Collections.emptyList());
        CalculatorCommand percentageCommand = new PercentageCommand();
        //when
        calculation = percentageCommand.execute(calculation);
        calculation.calculateResult();
        //then
        assertEquals(expected, calculation.getResult().getRaw());
    }

    @Test
    void should_do_nothing_when_value_of_element_is_zero() {
        //given
        Calculation calculation = new Calculation(Collections.singletonList(new NumberElement()),
                Collections.emptyList());
        CalculatorCommand percentageCommand = new PercentageCommand();
        String expected = "0";
        //when
        calculation = percentageCommand.execute(calculation);
        //then
        Element lastNumberElement = new LinkedList<>(calculation.getElements()).getLast();
        assertEquals(expected, lastNumberElement.asText());
    }

    @ParameterizedTest
    @CsvSource({"13, 0.13",
            "1, 0.01"})
    void should_affect_only_last_element(String element, String expected) {
        //given
        Calculation calculation = new Calculation(Arrays.asList(new NumberElement(), new NumberElement(element)),
                Collections.singletonList(Operations.ADDITION));
        CalculatorCommand percentageCommand = new PercentageCommand();
        //when
        calculation = percentageCommand.execute(calculation);
        //then
        Element lastNumberElement = new LinkedList<>(calculation.getElements()).getLast();
        assertEquals(expected, lastNumberElement.asText());
    }

    @Test
    void should_not_have_any_effect_when_calculation_has_calculated_result() {
        //given
        Calculation calculation = new Calculation(Arrays.asList(new NumberElement("2"), new NumberElement("13")),
                Collections.singletonList(Operations.ADDITION));
        CalculatorCommand percentageCommand = new PercentageCommand();
        //when
        calculation.calculateResult();
        calculation = percentageCommand.execute(calculation);
        //then
        Element lastNumberElement = new LinkedList<>(calculation.getElements()).getLast();
        assertEquals("13", lastNumberElement.asText());

    }

}