package eu.proszowski.democalculator.command;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import eu.proszowski.democalculator.TestUtils;
import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.Digit;
import eu.proszowski.democalculator.domain.element.Element;
import eu.proszowski.democalculator.domain.element.NumberElement;
import eu.proszowski.democalculator.domain.operation.Operations;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AppendDigitCommandTest {

    @Test
    void should_add_element_when_no_elements_in_calculation() {
        //given
        Calculation calculation = new Calculation();
        CalculatorCommand calculatorCommand = new AppendDigitCommand(Digit.ONE);
        //when
        Calculation result = calculatorCommand.execute(calculation);
        //then
        assertEquals(1, result.getElements().size());
    }

    @Test
    void should_add_element_when_there_is_the_same_amount_of_operations_and_elements() {
        //given
        Calculation calculation = new Calculation(Collections.singletonList(new NumberElement("123")),
                Collections.singletonList(Operations.ADDITION));
        CalculatorCommand calculatorCommand = new AppendDigitCommand(Digit.SEVEN);
        //when
        Calculation result = calculatorCommand.execute(calculation);
        //then
        assertEquals(2, result.getElements().size());
    }

    @Test
    void should_clear_elements_and_operations_when_result_was_calculated_and_new_digit_is_appended() {
        //given
        Calculation calculation = new Calculation(Arrays.asList(new NumberElement("1223"), new NumberElement("7839")),
                Collections.singletonList(Operations.ADDITION));
        CalculatorCommand appendDigit = new AppendDigitCommand(Digit.SEVEN);
        calculation.calculateResult();
        //when
        Calculation result = appendDigit.execute(calculation);
        //then
        assertEquals(1, result.getElements().size());
    }

    @Test
    void should_append_zero() {
        //given
        Calculation calculation = new Calculation(Arrays.asList(new NumberElement("1223"), new NumberElement("7839")),
                Collections.singletonList(Operations.ADDITION));
        CalculatorCommand appendDigit = new AppendDigitCommand(Digit.ZERO);
        //when
        calculation = appendDigit.execute(calculation);
        //then
        Element lastNumberElement = (TestUtils.getLastElement(calculation.getElements()));
        assertEquals(78390, lastNumberElement.getValue());
    }

}