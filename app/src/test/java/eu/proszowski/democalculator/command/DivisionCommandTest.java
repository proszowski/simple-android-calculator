package eu.proszowski.democalculator.command;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.Collections;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.Result;
import eu.proszowski.democalculator.domain.element.NumberElement;
import eu.proszowski.democalculator.domain.operation.Operations;

import static eu.proszowski.democalculator.TestUtils.givenOneElement;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DivisionCommandTest {

    @Test
    void should_add_division_sign_to_operations() {
        //given
        Calculation calculation = new Calculation(givenOneElement(), Collections.emptyList());
        CalculatorCommand divisionCommand = new DivisionCommand();
        //when
        calculation = divisionCommand.execute(calculation);
        //then
        assertTrue(calculation.getOperations().contains(Operations.DIVISION));
    }

    @ParameterizedTest
    @CsvSource({"5, 2, 2.5",
            "6.25, 2.5, 2.5"})
    void should_properly_perform_division(String first, String second, Double expected) {
        //given
        Calculation calculation = new Calculation(Arrays.asList(new NumberElement(first), new NumberElement(second)),
                Collections.singletonList(Operations.DIVISION));
        //when
        calculation.calculateResult();
        Result result = calculation.getResult();
        //then
        assertEquals(expected, result.getRaw());
    }

    @Test
    void should_update_error_message_in_calculation_when_division_by_zero_occurs() {
        //given
        Calculation calculation = new Calculation(Arrays.asList(new NumberElement("1"), new NumberElement()),
                Collections.singletonList(Operations.DIVISION));
        //when
        calculation.calculateResult();
        Result result = calculation.getResult();
        //then
        assertEquals("Can't divide by zero", result.getErrorMessage());
    }

}