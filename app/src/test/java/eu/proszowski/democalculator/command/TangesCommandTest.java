package eu.proszowski.democalculator.command;

import org.junit.jupiter.api.Test;

import java.util.Collections;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.Digit;
import eu.proszowski.democalculator.domain.Result;
import eu.proszowski.democalculator.domain.element.TangesElement;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TangesCommandTest {

    @Test
    void should_return_proper_tanges_value() {
        //given
        Calculation calculation = new Calculation(Collections.singletonList(new TangesElement()),
                Collections.emptyList());
        AppendDigitCommand appendDigitCommand = new AppendDigitCommand(Digit.THREE);
        calculation = appendDigitCommand.execute(calculation);
        //when
        calculation.calculateResult();
        Result result = calculation.getResult();
        //then
        assertEquals(Math.tan(3), result.getRaw());
    }

}