package eu.proszowski.democalculator.command;

import org.junit.jupiter.api.Test;

import java.util.Collections;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.Digit;
import eu.proszowski.democalculator.domain.Result;
import eu.proszowski.democalculator.domain.element.NumberElement;
import eu.proszowski.democalculator.domain.operation.Operations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SinusCommandTest {

    @Test
    void should_return_sinus_of_given_number() {
        //given
        Calculation calculation = new Calculation();
        SinusCommand sinusCommand = new SinusCommand();
        calculation = sinusCommand.execute(calculation);
        AppendDigitCommand appendDigitCommand = new AppendDigitCommand(Digit.EIGHT);
        calculation = appendDigitCommand.execute(calculation);
        //when
        calculation.calculateResult();
        //then
        Result result = calculation.getResult();
        assertEquals(Math.sin(8), result.getRaw());
    }

    @Test
    void should_append_multiplication_sign_when_appending_sinus_next_to_other_element() {
        //given
        Calculation calculation = new Calculation(Collections.singletonList(new NumberElement("12.3")),
                Collections.emptyList());
        SinusCommand sinusCommand = new SinusCommand();
        //when
        calculation = sinusCommand.execute(calculation);
        //then
        assertTrue(calculation.getOperations().contains(Operations.MULTIPLICATION));
    }

}