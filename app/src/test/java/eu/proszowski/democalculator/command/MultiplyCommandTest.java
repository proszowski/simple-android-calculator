package eu.proszowski.democalculator.command;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.Collections;

import eu.proszowski.democalculator.TestUtils;
import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.Result;
import eu.proszowski.democalculator.domain.element.NumberElement;
import eu.proszowski.democalculator.domain.operation.Operations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MultiplyCommandTest {

    @Test
    void should_add_multiplication_sign_to_operations() {
        //given
        Calculation calculation = new Calculation(TestUtils.givenOneElement(), Collections.emptyList());
        CalculatorCommand multiplyCommand = new MultiplyCommand();
        //when
        calculation = multiplyCommand.execute(calculation);
        //then
        assertTrue(calculation.getOperations().contains(Operations.MULTIPLICATION));
    }

    @ParameterizedTest(name = "{0} times {1} should be {2}")
    @CsvSource({"10, 5, 50",
            "2.5, 2.5, 6.25",
            "0, 102, 0",})
    void should_perform_multiplication(String first, String second, Double expected) {
        //given
        Calculation calculation = new Calculation(Arrays.asList(new NumberElement(first), new NumberElement(second)),
                Collections.singletonList(Operations.MULTIPLICATION));
        //when
        calculation.calculateResult();
        Result result = calculation.getResult();
        //then
        assertEquals(expected, result.getRaw());
    }

}