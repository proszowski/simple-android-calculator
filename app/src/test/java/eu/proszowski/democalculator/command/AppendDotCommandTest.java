package eu.proszowski.democalculator.command;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.NumberElement;
import eu.proszowski.democalculator.domain.operation.Operations;

import static eu.proszowski.democalculator.TestUtils.getLastElement;
import static org.junit.jupiter.api.Assertions.assertEquals;

class AppendDotCommandTest {

    @Test
    void should_increase_length_of_last_element_when_append_dot() {
        //given
        CalculatorCommand appendDot = new AppendDotCommand();
        Calculation calculation = new Calculation();
        //when
        int before = getLastElement(calculation.getElements()).asText().length();
        calculation = appendDot.execute(calculation);
        int after = getLastElement(calculation.getElements()).asText().length();
        //then
        assertEquals(before + 1, after);
    }

    @Test
    void should_not_increase_length_of_last_element_if_dot_already_exists() {
        //given
        CalculatorCommand appendDot = new AppendDotCommand();
        Calculation calculation = new Calculation();
        //when
        int before = getLastElement(calculation.getElements()).asText().length();
        calculation = appendDot.execute(calculation);
        calculation = appendDot.execute(calculation);
        int after = getLastElement(calculation.getElements()).asText().length();
        //then
        assertEquals(before + 1, after);
    }

    @Test
    void should_clear_elements_and_operations_when_result_was_calculated_and_new_dot_is_appended() {
        //given
        Calculation calculation = new Calculation(Arrays.asList(new NumberElement("1223"), new NumberElement("7839")),
                Collections.singletonList(Operations.ADDITION));
        CalculatorCommand appendDot = new AppendDotCommand();
        calculation.calculateResult();
        //when
        calculation = appendDot.execute(calculation);
        //then
        assertEquals(1, calculation.getElements().size());
    }

    @Test
    void should_create_new_element_if_there_is_the_same_amount_of_elements_and_operations() {
        //given
        Calculation calculation = new Calculation(Collections.singletonList(new NumberElement("1223")),
                Collections.singletonList(Operations.ADDITION));
        CalculatorCommand appendDot = new AppendDotCommand();
        //when
        calculation = appendDot.execute(calculation);
        //then
        assertEquals(2, calculation.getElements().size());
    }

}