package eu.proszowski.democalculator.command;

import org.junit.jupiter.api.Test;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.Digit;
import eu.proszowski.democalculator.domain.Result;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SquareRootCommandTest {

    @Test
    void should_calculate_square_root_properly() {
        //given
        Calculation calculation = new Calculation();
        SquareRootCommand sinusCommand = new SquareRootCommand();
        calculation = sinusCommand.execute(calculation);
        AppendDigitCommand appendDigitCommand = new AppendDigitCommand(Digit.SIX);
        calculation = appendDigitCommand.execute(calculation);
        //when
        calculation.calculateResult();
        Result result = calculation.getResult();
        //then
        assertEquals(Math.sqrt(6), result.getRaw());
    }

}