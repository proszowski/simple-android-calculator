package eu.proszowski.democalculator.command;

import org.junit.jupiter.api.Test;

import java.util.Collections;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.Digit;
import eu.proszowski.democalculator.domain.Result;
import eu.proszowski.democalculator.domain.element.CosinusElement;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CosinusCommandTest {

    @Test
    void should_properly_calculate_cosinus() {
        //given
        Calculation calculation = new Calculation(Collections.singletonList(new CosinusElement()),
                Collections.emptyList());
        AppendDigitCommand appendDigitCommand = new AppendDigitCommand(Digit.SIX);
        calculation = appendDigitCommand.execute(calculation);
        //when
        calculation.calculateResult();
        Result result = calculation.getResult();
        //then
        assertEquals(Math.cos(6), result.getRaw());
    }

}