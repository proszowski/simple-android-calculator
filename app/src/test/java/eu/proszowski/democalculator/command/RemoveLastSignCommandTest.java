package eu.proszowski.democalculator.command;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.Digit;
import eu.proszowski.democalculator.domain.element.Element;
import eu.proszowski.democalculator.domain.element.NumberElement;
import eu.proszowski.democalculator.domain.element.SinusElement;
import eu.proszowski.democalculator.domain.operation.Operations;

import static eu.proszowski.democalculator.TestUtils.getLastElement;
import static eu.proszowski.democalculator.TestUtils.givenOneElement;
import static eu.proszowski.democalculator.TestUtils.givenOneOperation;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RemoveLastSignCommandTest {

    @Test
    void should_remove_operation_if_operation_was_provided_last_time() {
        //given
        Calculation calculation = new Calculation(givenOneElement(), givenOneOperation());
        CalculatorCommand removeLastSign = new RemoveLastSignCommand();
        //when
        Calculation result = removeLastSign.execute(calculation);
        //then
        assertEquals(Collections.emptyList(), result.getOperations());

    }

    @Test
    void should_remove_digit_if_digit_was_provided_last_time() {
        //given
        Calculation calculation = new Calculation(givenOneElement(), Collections.emptyList());
        CalculatorCommand removeLastSign = new RemoveLastSignCommand();
        int lengthOfLastElementBeforeCommand = getLastElement(calculation.getElements()).asText().length();
        //when
        calculation = removeLastSign.execute(calculation);
        int lengthOfLastElementAfterCommand = getLastElement(calculation.getElements()).asText().length();
        //then
        assertEquals(lengthOfLastElementBeforeCommand - 1, lengthOfLastElementAfterCommand);
    }

    @Test
    void should_not_have_any_influence_when_no_element_exist_in_calculation() {
        //given
        Calculation calculation = new Calculation(Collections.emptyList(), Collections.emptyList());
        CalculatorCommand removeLastSign = new RemoveLastSignCommand();
        CalculatorCommand appendDigit = new AppendDigitCommand(Digit.EIGHT);
        //when
        calculation = removeLastSign.execute(calculation);
        calculation = appendDigit.execute(calculation);
        calculation = removeLastSign.execute(calculation);
        int lengthOfLastElementAfterCommand = getLastElement(calculation.getElements()).asText().length();
        //then
        assertEquals(1, lengthOfLastElementAfterCommand);
    }


    @Test
    void should_remove_dot_if_dot_was_provided_last_time() {
        //given
        Calculation calculation = new Calculation(givenOneElement(), Collections.emptyList());
        CalculatorCommand removeLastSign = new RemoveLastSignCommand();
        CalculatorCommand appendDot = new AppendDotCommand();
        calculation = appendDot.execute(calculation);
        int lengthOfLastElementBeforeCommand = getLastElement(calculation.getElements()).asText().length();
        //when
        calculation = removeLastSign.execute(calculation);
        int lengthOfLastElementAfterCommand = getLastElement(calculation.getElements()).asText().length();
        //then
        assertEquals(lengthOfLastElementBeforeCommand - 1, lengthOfLastElementAfterCommand);
    }

    @Test
    void should_replace_last_sign_with_zero() {
        //given
        Calculation calculation = new Calculation(Collections.singletonList(new NumberElement("1")),
                Collections.emptyList());
        CalculatorCommand removeLastSign = new RemoveLastSignCommand();
        //when
        calculation = removeLastSign.execute(calculation);
        //then
        Element lastNumberElement = getLastElement(calculation.getElements());
        assertEquals(0, lastNumberElement.getValue());
    }

    @Test
    void should_remove_last_operation_sign_if_exist() {
        //given
        Calculation calculation = new Calculation(Collections.singletonList(new NumberElement("1")),
                Collections.singletonList(Operations.SUBTRACTION));
        CalculatorCommand removeLastSign = new RemoveLastSignCommand();
        //when
        calculation = removeLastSign.execute(calculation);
        //then
        assertTrue(calculation.getOperations().isEmpty());
    }

    @Test
    void should_remove_last_sign_if_there_is_operation_sign_in_calculation() {
        //given
        Calculation calculation = new Calculation(Arrays.asList(new NumberElement("1"), new NumberElement("2")),
                Collections.singletonList(Operations.SUBTRACTION));
        CalculatorCommand removeLastSign = new RemoveLastSignCommand();
        //when
        calculation = removeLastSign.execute(calculation);
        //then
        assertEquals(1, calculation.getElements().size());
    }

    @Test
    void should_remove_last_element_if_it_is_trigonometric_element() {
        //given
        Calculation calculation = new Calculation(Collections.singletonList(new SinusElement()),
                Collections.emptyList());
        CalculatorCommand removeLastSign = new RemoveLastSignCommand();
        //when
        calculation = removeLastSign.execute(calculation);
        //then
        Element lastElement = getLastElement(calculation.getElements());
        assertEquals("0", lastElement.asText());
    }
}