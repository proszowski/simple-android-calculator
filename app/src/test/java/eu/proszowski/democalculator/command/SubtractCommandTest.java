package eu.proszowski.democalculator.command;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.NumberElement;
import eu.proszowski.democalculator.domain.operation.Operations;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SubtractCommandTest {

    @Test
    void should_subtract_two_numbers() {
        //given
        Calculation calculation = new Calculation(Arrays.asList(new NumberElement(), new NumberElement("32"),
                new NumberElement("-2")),
                Arrays.asList(Operations.SUBTRACTION, Operations.SUBTRACTION));
        CalculatorCommand subtractCommand = new SubtractCommand();
        //when
        calculation = subtractCommand.execute(calculation);
        calculation.calculateResult();
        //then
        assertEquals(-30, calculation.getResult().getRaw());
    }

}