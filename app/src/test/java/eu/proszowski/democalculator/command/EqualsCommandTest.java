package eu.proszowski.democalculator.command;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.Collections;

import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.NumberElement;
import eu.proszowski.democalculator.domain.operation.Operations;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EqualsCommandTest {

    @Test
    void should_calculate_result() {
        //given
        Calculation calculation = new Calculation(Arrays.asList(new NumberElement("1.23"), new NumberElement("2.77"),
                new NumberElement("16")), Arrays.asList(Operations.ADDITION, Operations.ADDITION));
        CalculatorCommand equalsCommand = new EqualsCommand();
        //when
        calculation = equalsCommand.execute(calculation);
        //then
        assertEquals(1.23 + 2.77 + 16, calculation.getResult().getRaw());
    }

    @ParameterizedTest
    @CsvSource({"987654321, 987654321, 9.754610578e17",
            "123123.123123, 123123.123123, 1.515930345e10",
            "123123.123123, -123123.123123, -1.515930345e10"
    })
    void should_calculate_result_when_numbers_with_big_number_of_digits_are_calculated(String first, String second,
                                                                                       String expected) {
        //given
        Calculation calculation = new Calculation(Arrays.asList(new NumberElement(first), new NumberElement(second)),
                Collections.singletonList(Operations.MULTIPLICATION));
        CalculatorCommand equalsCommand = new EqualsCommand();
        //when
        calculation = equalsCommand.execute(calculation);
        //then
        assertEquals(expected, calculation.getResult().asText());
    }

    @Test
    void should_add_element_if_amount_of_operations_is_the_same_as_amount_of_elements() {
        //given
        Calculation calculation = new Calculation(Arrays.asList(new NumberElement(), new NumberElement("1")),
                Arrays.asList(Operations.MULTIPLICATION, Operations.ADDITION));
        CalculatorCommand equalsCommand = new EqualsCommand();
        //when
        calculation = equalsCommand.execute(calculation);
        //then
        assertEquals(3, calculation.getElements().size());
    }

}