package eu.proszowski.democalculator.command;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import eu.proszowski.democalculator.TestUtils;
import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.Result;
import eu.proszowski.democalculator.domain.element.Element;
import eu.proszowski.democalculator.domain.element.FactorialElement;
import eu.proszowski.democalculator.domain.element.NumberElement;
import eu.proszowski.democalculator.domain.element.SinusElement;
import eu.proszowski.democalculator.domain.operation.Operations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class FactorialCommandTest {

    @Test
    void should_calculate_factor_of_last_element() {
        //given
        Calculation calculation = new Calculation(Collections.singletonList(new NumberElement("6")),
                Collections.emptyList());
        FactorialCommand factorialCommand = new FactorialCommand();
        //when
        calculation = factorialCommand.execute(calculation);
        calculation.calculateResult();
        //then
        Result result = calculation.getResult();
        assertEquals(6 * 5 * 4 * 3 * 2, result.getRaw());
    }

    @Test
    void should_have_message_about_error_when_decimal_number_provided() {
        //given
        Calculation calculation = new Calculation(Collections.singletonList(new NumberElement("6.423")),
                Collections.emptyList());
        FactorialCommand factorialCommand = new FactorialCommand();
        //when
        calculation = factorialCommand.execute(calculation);
        calculation.calculateResult();
        //then
        Result result = calculation.getResult();
        assertNotNull(result.getErrorMessage());
    }

    @Test
    void should_keep_order_of_calculation() {
        //given
        Calculation calculation = new Calculation(Arrays.asList(new NumberElement("6.423"),
                new SinusElement(), new NumberElement("534")),
                Arrays.asList(Operations.ADDITION, Operations.SUBTRACTION));
        FactorialCommand factorialCommand = new FactorialCommand();
        //when
        calculation = factorialCommand.execute(calculation);
        //then
        List<Element> elements = calculation.getElements();
        assertEquals("6.423", elements.get(0).asText());
        assertEquals("sin(0)", elements.get(1).asText());
        assertEquals("534!", elements.get(2).asText());
    }

    @Test
    void should_not_add_another_factorial_sign_when_last_element_is_factorial() {
        //given
        Calculation calculation = new Calculation(Collections.singletonList(new FactorialElement()),
                Collections.emptyList());
        FactorialCommand factorialCommand = new FactorialCommand();
        //when
        calculation = factorialCommand.execute(calculation);
        //then
        List<Element> elements = calculation.getElements();
        Element lastElement = TestUtils.getLastElement(elements);
        assertEquals("0!", lastElement.asText());
    }

}