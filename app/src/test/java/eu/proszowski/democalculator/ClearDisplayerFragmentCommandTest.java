package eu.proszowski.democalculator;

import org.junit.jupiter.api.Test;

import java.util.Collections;

import eu.proszowski.democalculator.command.CalculatorCommand;
import eu.proszowski.democalculator.command.ClearDisplayerCommand;
import eu.proszowski.democalculator.domain.Calculation;
import eu.proszowski.democalculator.domain.element.NumberElement;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ClearDisplayerFragmentCommandTest {

    @Test
    void should_clear_calculation() {
        //given
        Calculation calculation = new Calculation();
        CalculatorCommand clearDisplayer = new ClearDisplayerCommand();
        //when
        Calculation result = clearDisplayer.execute(calculation);
        //then
        assertEquals(Collections.singletonList(new NumberElement()), result.getElements());
        assertEquals(Collections.emptyList(), result.getOperations());
    }

}